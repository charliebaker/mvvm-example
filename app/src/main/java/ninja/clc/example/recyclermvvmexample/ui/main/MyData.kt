package ninja.clc.example.recyclermvvmexample.ui.main

data class MyData(
    val title: String,
    val names: ArrayList<String>
)