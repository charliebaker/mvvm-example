package ninja.clc.example.recyclermvvmexample.ui.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ninja.clc.example.recyclermvvmexample.databinding.ItemRecycleBinding

class MvvmRecyclerAdapter (
    // List of backing data...
    private val data: ArrayList<MyData>
): RecyclerView.Adapter<MvvmRecyclerAdapter.MvvmViewHolder>() {

    // Recycler uses the viewModels though for each row.
    private val viewModels = arrayListOf<MvvmItemViewModel>()
    private val viewPool = RecyclerView.RecycledViewPool()

    init {
        // being lazy, this could be done in a setData method I guess...
        for (d in data) {
            // Probably would use dagger or something to get a new instance of this.
            val itemViewModel = MvvmItemViewModel()
            itemViewModel.setData(d)
            viewModels.add(itemViewModel)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MvvmViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemRecycleBinding.inflate(inflater, null, false)
        return MvvmViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: MvvmViewHolder, position: Int) {
        holder.bindViewModel(viewModels[position], viewPool)
    }

    class MvvmViewHolder(private val binding: ItemRecycleBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bindViewModel(viewModel: MvvmItemViewModel, viewPool: RecyclerView.RecycledViewPool) {
            binding.itemViewModel = viewModel


            val recycler = binding.internalRecycler
            recycler.layoutManager = LinearLayoutManager(recycler.context, LinearLayoutManager.HORIZONTAL, false)
            recycler.setRecycledViewPool(viewPool)
            recycler.adapter = viewModel.adapter

            binding.executePendingBindings()
        }

    }

}