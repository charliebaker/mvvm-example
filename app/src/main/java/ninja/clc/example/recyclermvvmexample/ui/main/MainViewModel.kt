package ninja.clc.example.recyclermvvmexample.ui.main

import android.util.Log
import androidx.lifecycle.*

class MainViewModel : ViewModel(), LifecycleObserver {

    val message = MutableLiveData<String>("Hi Liam")

    val adapter = MutableLiveData<MvvmRecyclerAdapter>()

    fun setLifecycle(lifecycle: Lifecycle) {
        lifecycle.addObserver(this)
    }


    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun onResume() {

        val myData = arrayListOf<MyData>(
            MyData("One", arrayListOf("Charlie", "John", "James")),
            MyData("Two", arrayListOf("Ioana", "Chrys")),
            MyData("Three", arrayListOf("George")),
            MyData("Four", arrayListOf("4", "Francesca", "Callum", "Louis")),
            MyData("5", arrayListOf("5", "Charlie", "John", "James")),
            MyData("6", arrayListOf("6", "Ioana", "Chrys")),
            MyData("7", arrayListOf("7", "George")),
            MyData("8", arrayListOf("8", "Francesca", "Callum", "Louis")),
            MyData("One", arrayListOf("One", "Charlie", "John", "James")),
            MyData("999", arrayListOf("9", "Ioana", "Chrys")),
            MyData("Ten", arrayListOf("10", "George")),
            MyData("11", arrayListOf("11", "Francesca", "Callum", "Louis"))
        )

        adapter.value = MvvmRecyclerAdapter(myData)
    }

}
