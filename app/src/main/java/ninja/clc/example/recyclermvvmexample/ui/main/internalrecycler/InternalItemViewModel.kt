package ninja.clc.example.recyclermvvmexample.ui.main.internalrecycler

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class InternalItemViewModel : ViewModel() {

    val name = MutableLiveData<String>("INTERNAL Default Label")

    fun setName(text: String) {
        name.value = text
    }

    fun itemClicked() {
        Log.v("Charlie", "Internal Item ${name.value} Clicked! Do something here if you want...")
    }

}