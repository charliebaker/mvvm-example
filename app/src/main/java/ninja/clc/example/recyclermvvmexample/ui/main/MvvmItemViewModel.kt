package ninja.clc.example.recyclermvvmexample.ui.main

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import ninja.clc.example.recyclermvvmexample.ui.main.internalrecycler.InternalRecyclerAdapter

class MvvmItemViewModel : ViewModel() {

    val itemText = MutableLiveData<String>("Default Label")

    lateinit var adapter: InternalRecyclerAdapter

    fun setData(text: MyData) {
        itemText.value = text.title
        adapter = InternalRecyclerAdapter(text.names)
    }

    fun itemClicked() {
        Log.v("Charlie", "Item ${itemText.value} Clicked! Do something here if you want...")
    }

}