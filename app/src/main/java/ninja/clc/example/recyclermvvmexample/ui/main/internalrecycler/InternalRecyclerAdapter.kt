package ninja.clc.example.recyclermvvmexample.ui.main.internalrecycler

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_recycle.view.*
import ninja.clc.example.recyclermvvmexample.databinding.ItemInternalBinding
import ninja.clc.example.recyclermvvmexample.databinding.ItemRecycleBinding

class InternalRecyclerAdapter (
    // List of backing data...
    private val data: ArrayList<String>
): RecyclerView.Adapter<InternalRecyclerAdapter.InternalMvvmViewHolder>() {

    // Recycler uses the viewModels though for each row.
    private val viewModels = arrayListOf<InternalItemViewModel>()

    init {
        // being lazy, this could be done in a setData method I guess...
        for (d in data) {
            // Probably would use dagger or something to get a new instance of this.
            val itemViewModel = InternalItemViewModel()
            itemViewModel.setName(d)
            viewModels.add(itemViewModel)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InternalMvvmViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemInternalBinding.inflate(inflater, null, false)
        return InternalMvvmViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: InternalMvvmViewHolder, position: Int) {
        holder.bindViewModel(viewModels[position])
    }

    class InternalMvvmViewHolder(private val binding: ItemInternalBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bindViewModel(viewModel: InternalItemViewModel) {
            binding.itemViewModel = viewModel
            binding.executePendingBindings()
        }

    }

}