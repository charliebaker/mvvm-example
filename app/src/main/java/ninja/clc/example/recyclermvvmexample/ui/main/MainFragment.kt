package ninja.clc.example.recyclermvvmexample.ui.main

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_main.*
import ninja.clc.example.recyclermvvmexample.databinding.FragmentMainBinding

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var viewModel: MainViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        // Normally would be injected.
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)

        val binding = FragmentMainBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        viewModel.setLifecycle(lifecycle)

        viewModel.adapter.observe(viewLifecycleOwner, Observer {
            recycler.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
            recycler.adapter = it
        })

        return binding.root
    }
}
